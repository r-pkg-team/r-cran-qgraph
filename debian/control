Source: r-cran-qgraph
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp,
               r-cran-psych,
               r-cran-lavaan,
               r-cran-plyr,
               r-cran-hmisc,
               r-cran-igraph,
               r-cran-jpeg,
               r-cran-png,
               r-cran-colorspace,
               r-cran-matrix,
               r-cran-corpcor,
               r-cran-reshape2,
               r-cran-ggplot2,
               r-cran-glasso,
               r-cran-fdrtool,
               r-cran-gtools,
               r-cran-pbapply,
               r-cran-abind
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-qgraph
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-qgraph.git
Homepage: https://cran.r-project.org/package=qgraph
Rules-Requires-Root: no

Package: r-cran-qgraph
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R graph plotting methods and psychometric data visualization
 This GNU R package provides graph plotting methods, psychometric data
 visualization and graphical model estimation functions based on
 weighted network visualization and analysis, as well as Gaussian graphical
 model computation. See Epskamp et al. (2012) <doi:10.18637/jss.v048.i04>.
